source-config:
  file.managed:
    {% if grains['osfullname'] == 'Ubuntu' and grains['osrelease'] == '16.04' %}
    - name: /etc/apt/sources.list
    - source: salt://linux-source/files/neteasy-ubuntu16.list
    {% endif %}
    - template: jinja
    - mode: 644
    - user: root
    - group: root

source-update:
  cmd.run:
    {% if grains['osfullname'] == 'Ubuntu' %}
    - name: apt-get -y update
    {% endif %}